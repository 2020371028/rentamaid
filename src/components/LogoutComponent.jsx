import React from "react";

export default function LogoutComponent() {
    return (
        <div className="LogoutComponent">
            <h1>You are logged out</h1>
            <div>Until next time padrino!!!</div>
        </div>
    );
}
