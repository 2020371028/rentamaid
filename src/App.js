import "./App.css";
import RentAMaidApp from "./components/RentAMaidApp";

export default function App() {
    return (
        <div className="App">
            <RentAMaidApp />
        </div>
    );
}
